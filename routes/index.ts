import {Request, Response, Router} from 'express';
import Correo from "./correo/enviar-correo";
import ClienteRoutes from "./ClienteRoutes";
import EmailRoutes from "./correo/EmailRoutes";
import AppsRouter from "./AppsRoutes";
import recibosRoutes from "./recibosRoutes";
const routes = Router();

routes.use('/mailing', Correo);
routes.use('/auth-cliente', ClienteRoutes);
routes.use('/v1/cliente', ClienteRoutes);
routes.use('/v1/app-cliente', ClienteRoutes);
routes.use('/v1/apps/email-api', EmailRoutes);
routes.use('/v1/apps', AppsRouter);
routes.use('/v1/recibos',recibosRoutes);
// routes.use('/v1/chat', chatRoutes);

export default routes;
