import {Request, Response, Router} from "express";
import RecibosView from "../models/RecibosViewModel";

const router = Router();

router.get('/:idRegistro', async(req: Request, res: Response) => {
    try {
        const recibos = await RecibosView.findAll({
            where: {
                idRegistro: req.params.idRegistro
            },
            order: [['numero', 'ASC']]
        });
        res.json(recibos);
    } catch (e) {
        res.status(500).json(e);
    }
});

export default router;