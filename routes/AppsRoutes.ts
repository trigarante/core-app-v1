import {Request, Response, Router} from "express";
import axios from "axios";
import AppsService from "../services/AppsService";
import SeguimientoMailService from "../services/SeguimientoMailService";
import EmailApi from "./correo/emailApi";
import ClienteService from "../services/ClienteService";
import PasswordEmail from "./correo/passwordEmail";

const AppsRouter = Router();

AppsRouter.get('/logueos-apps', async (req: Request, res: Response, next) => {
    try {
        const logueos = await AppsService.getAllLogueoApp();
        res.status(200).send(logueos);
    } catch (err) {
        next(err);
    }
});

AppsRouter.get('/descargas-polizas', async (req: Request, res: Response, next) => {
    try {
        const descargas = await AppsService.getAllDescargasPoliza();
        res.status(200).send(descargas);
    } catch (err) {
        next(err);
    }
});

AppsRouter.get('/seguimiento-mail', async (req: Request, res: Response, next) => {
    try {
        const mail = await AppsService.getMailSeguimiento();
        res.status(200).send(mail);
    } catch (err) {
        next(err);
    }
});

AppsRouter.get('/reporte', async (req: Request, res: Response, next) => {
    try {
        const mail = await AppsService.getReporteGlobal();
        res.status(200).send(mail);
    } catch (err) {
        next(err);
    }
});

AppsRouter.get('/empresas', async (req: Request, res: Response, next) => {
    try {
        const mail = await AppsService.getEmpresas();
        res.status(200).send(mail);
    } catch (err) {
        next(err);
    }
});

AppsRouter.get('/reporte/:idEmpresa', async (req: Request, res: Response, next) => {
    try {
        const mail = await AppsService.getReporteByIdEmpresa(req.params.idEmpresa);
        res.status(200).send(mail);
    } catch (err) {
        next(err);
    }
});

AppsRouter.get('/recuperar-email/:idCliente/:idEmpresa', async (req: Request, res: Response, next) => {
    try {
        const mail = EmailApi.sendPasswordEmail(req.params.idCliente, req.params.idEmpresa);
        return res.json(mail);
    } catch (err) {
        next(err);
    }
});

AppsRouter.post('/reset-password', async (req: Request, res: Response, next) => {
    try {
        const recovery = await SeguimientoMailService.findRecoveryById(req.body.id);
        const cliente = await ClienteService.updatePassword(recovery.idCliente, req.body.contrasenaApp);
        const dataUpdate = {
            activo: 0
        };
        const recoveryUpdate = await SeguimientoMailService.updateRecoveryPassword(dataUpdate, recovery.id);
        res.status(200).send(recovery);
    } catch (err) {
        next(err);
    }
});

export default AppsRouter;
