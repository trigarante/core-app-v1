import {Request, Response, Router} from "express";
import EmailApi from "./emailApi";
import SeguimientoMailService from "../../services/SeguimientoMailService";
import CanalWhatsappService from "../../services/CanalWhatsappService";
import MarcasEmpresaService from "../../services/MarcasEmpresaService";

const EmailRoutes = Router();
EmailRoutes.post('/webhook', async (req: Request, res: Response, next) => {
    // const mandrillEvents = JSON.parse(req.body.mandrill_events);
    const {type, data} = req.body;
    if (type === "subscribe") {
        await SeguimientoMailService.webhookMailchimp(data);
    } else if (type === "unsubscribe") {
        await SeguimientoMailService.webhookMailchimp(data);
    }
    res.send("Done");
});
EmailRoutes.get('/test-mailchimp', async (req: Request, res: Response, next) => {
    const mailchimpFactory = require("@mailchimp/mailchimp_transactional/src/index.js");
    const mailchimp = mailchimpFactory("Y0s4Hh4u5Id7pLP84bPmDg");
    // const info = await mailchimp.messages.info({id: "060f3ec93d444e619e8a30c214ddb50a"});
    // console.log(info);
    const message = {
        from_email: "no-reply@info-ahorraseguros.com",
        subject: "Hello world",
        text: "Welcome to Mailchimp Transactional!   https://trigarante2020.com",
        to: [
            {
                email: "noreply@info-ahorraseguros.com",
                type: "to"
            }
        ],
        track_opens: true,
        track_clicks: true
    };
    const response = await mailchimp.messages.sendTemplate({
        template_name: "VN_No contactado_03",
        template_content: [{}],
        message
    });
    console.log(response);
});

EmailRoutes.get('/:idCliente', async (req: Request, res: Response, next) => {
    try {
        const email = EmailApi.sendEmail(req.params.idCliente);
        return res.json(email);
    } catch (err) {
        next(err);
    }
});


EmailRoutes.get('/tracking/:idEmail/:link/:idClientes', async (req: Request, res: Response, next) => {
    try {
        const marcaEmpresa: any = await MarcasEmpresaService.getByIdRegistro(req.params.idClientes);
        const linkWebApp = marcaEmpresa[0].linkWebApp;
        const linkAndroid = marcaEmpresa[0].linkAndroid;
        const linkIos = marcaEmpresa[0].linkIos;
        if (req.params.link == 'webapp') {
            const data = {
                abierto: 1,
                clickEnlaceWebApp: 1,
                recibido: 1
            };
            const email = SeguimientoMailService.update(data, req.params.idEmail);
            res.redirect(linkWebApp);
        }
        if (req.params.link == 'android') {
            const data = {
                abierto: 1,
                clickEnlaceAndroid: 1,
                recibido: 1
            };
            const email = SeguimientoMailService.update(data, req.params.idEmail);
            res.redirect(linkAndroid);
        }
        if (req.params.link == 'ios') {
            const data = {
                abierto: 1,
                clickEnlaceIos: 1,
                recibido: 1
            };
            const email = SeguimientoMailService.update(data, req.params.idEmail);
            res.redirect(linkIos);
        }
    } catch (err) {
        next(err);
    }
});

export default EmailRoutes;
