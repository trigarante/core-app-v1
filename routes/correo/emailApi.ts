import * as fs from "fs";
import {google} from "googleapis";
import * as readline from "readline";
import SeguimientoMailService from "../../services/SeguimientoMailService";
import ClienteService from "../../services/ClienteService";
import CanalWhatsappService from "../../services/CanalWhatsappService";
import MarcasEmpresaService from "../../services/MarcasEmpresaService";
import emailConfig from "../../config/emailConfig";
import {json} from "express";

export default class EmailApi {

    private static loadClienteSecret(idEmpresa): Promise<any> {
        return new Promise((resolve, reject) => {
            switch (idEmpresa) {
                case 1:
                    fs.readFile('config/credentials.json', (err, content) => {
                        if (err) reject(console.log('Error loading client secret file:', err));
                        // Authorize a client with credentials, then call the Google drive API.
                        resolve(JSON.parse(content.toString()).installed);
                    })
                    break;
                case 3:
                    fs.readFile('config/credentials-mejor-seguro.json', (err, content) => {
                        if (err) reject(console.log('Error loading client secret file:', err));
                        // Authorize a client with credentials, then call the Google drive API.
                        resolve(JSON.parse(content.toString()).installed);
                    })
                    break;
                case 5:
                    fs.readFile('config/credentials-migo.json', (err, content) => {
                        if (err) reject(console.log('Error loading client secret file:', err));
                        // Authorize a client with credentials, then call the Google drive API.
                        resolve(JSON.parse(content.toString()).installed);
                    })
                    break;
            }
        });
    }

    private static async authorize(scope, idMarcaEmpresa): Promise<any> {
        const {client_secret, client_id, redirect_uris} = await this.loadClienteSecret(idMarcaEmpresa);
        const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
        let scopes;
        let path = 'config/';

        switch (idMarcaEmpresa) {
            case 1:
                path += 'token.json';
                scopes = ['https://mail.google.com/',
                    'https://www.googleapis.com/auth/gmail.modify',
                    'https://www.googleapis.com/auth/gmail.compose',
                    'https://www.googleapis.com/auth/gmail.send'];
                break;
            case 2:
                path += 'tokenSend.json';
                scopes = ['https://www.googleapis.com/auth/gmail.send'];
                break;
            case 3:
                path += 'token-mejor-seguro.json';
                scopes = ['https://mail.google.com/',
                    'https://www.googleapis.com/auth/gmail.modify',
                    'https://www.googleapis.com/auth/gmail.compose',
                    'https://www.googleapis.com/auth/gmail.send'];
                break;
            case 4:
                path += 'tokenCompose.json';
                scopes = ['https://www.googleapis.com/auth/gmail.compose'];
                break;
            case 5:
                path += 'token-migo.json';
                scopes = ['https://mail.google.com/',
                    'https://www.googleapis.com/auth/gmail.modify',
                    'https://www.googleapis.com/auth/gmail.compose',
                    'https://www.googleapis.com/auth/gmail.send'];
                break;
        }

        return new Promise((resolve) => {
            // Check if we have previously stored a token.
            fs.readFile(path, (err, token) => {
                if (err) {
                    resolve(EmailApi.getAccessToken(oAuth2Client, scopes, path, idMarcaEmpresa));
                } else {
                    oAuth2Client.setCredentials(JSON.parse(token.toString()));
                    resolve(oAuth2Client);
                }
            });
        });
    }

    private static async getAccessToken(oAuth2Client, scopes, path, idMarcaEmpresa): Promise<any> {
        return new Promise(resolve => {
            const authUrl = oAuth2Client.generateAuthUrl({
                access_type: 'offline',
                scope: scopes,
            });
            console.log('Authorize this app by visiting this url:', authUrl);
            const rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout,
            });
            rl.question('Enter the code from that page here: ', (code) => {
                rl.close();
                oAuth2Client.getToken(code, (err, token) => {
                    if (err) console.error('Error retrieving access token', err);
                    oAuth2Client.setCredentials(token);
                    fs.writeFile('config/token-migo.json', JSON.stringify(token), (error) => {
                        if (error) console.error(error);
                        console.log('Token stored to', path);
                        resolve(oAuth2Client);
                    });
                });
            });
        });
    }

    static async sendPasswordEmail(idClienteApp, idMarca) {
        const auth = await EmailApi.authorize(5, 5);
        const marcaEmpresa = await MarcasEmpresaService.getById(idMarca);
        const linkWebApp = marcaEmpresa[0].linkWebApp;
        const linkAndroid = marcaEmpresa[0].linkAndroid;
        const linkIos = marcaEmpresa[0].linkIos;
        const bannerEmail = marcaEmpresa[0].bannerEmail;
        const cliente = await ClienteService.getById(idClienteApp);

        const dataRecovery = {
            idCliente: idClienteApp,
            idMarcaEmpresa: idMarca,
            activo: 1,
        };
        const passRecovery = await SeguimientoMailService.recoveryPassword(dataRecovery);
        const enlace = `localhost:8000/#/forgot-password/${Buffer.from('' + passRecovery.id).toString('base64')}`;
        const dataUpdateRecovery = {
            url: enlace
        };
        await SeguimientoMailService.updateRecoveryPassword(dataUpdateRecovery, passRecovery.id)
        return await emailConfig.transporter.sendMail({
            from: `${marcaEmpresa[0].descripcionEmail} <${marcaEmpresa[0].emailSalida}>`,
            to: cliente.correo,
            subject: `Bienvenido a ${marcaEmpresa[0].descripcionEmail}`,
            text: `Dirigete al siguiente link para recuperar tu contraseña: ${enlace}`,
            headers: {'default-encoding': 'UTF-8'}
        });
    }

    static async sendEmail(idClientes) {
        const url = 'https://app.mark-43.net';
        const marcaEmpresa: any = await MarcasEmpresaService.getByIdRegistro(idClientes);
        const linkWebApp = marcaEmpresa[0].linkWebApp;
        const linkAndroid = marcaEmpresa[0].linkAndroid;
        const linkIos = marcaEmpresa[0].linkIos;
        const bannerEmail = marcaEmpresa[0].bannerEmail;
        const dataMail: any = await SeguimientoMailService.getDataMail(marcaEmpresa[0].idSolicitud);
        const cliente = await ClienteService.getById(dataMail[0].idCliente);
        let password = '';
        if (cliente.contrasenaApp == null) {
            password = Math.random().toString(36).slice(-8);
            await ClienteService.updatePassword(dataMail[0].idCliente, password);
        } else {
            password = cliente.contrasenaApp;
        }
        const data1 = {
            idCliente: dataMail[0].idCliente,
            idMarcaEmpresa: marcaEmpresa[0].idMarcasEmpresa,
            idRegistro: idClientes,
            enviado: 1
        };
        const seguimientoMail = await SeguimientoMailService.save(data1);
        return await emailConfig.transporter.sendMail({
            from: `${marcaEmpresa[0].descripcionEmail} <${marcaEmpresa[0].emailSalida}>`,
            to: dataMail[0].correo,
            subject: `Bienvenido a ${marcaEmpresa[0].descripcionEmail}`,
            html: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
 <head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta name="x-apple-disable-message-reformatting">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="telephone=no" name="format-detection">
  <title>Nueva plantilla 1</title>
  <!--[if (mso 16)]>
    <style type="text/css">
    a {text-decoration: none;}
    </style>
    <![endif]-->
  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
  <!--[if gte mso 9]>
<xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG></o:AllowPNG>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
</xml>
<![endif]-->
  <style type="text/css">
#outlook a {
	padding:0;
}
.es-button {
	mso-style-priority:100!important;
	text-decoration:none!important;
}
a[x-apple-data-detectors] {
	color:inherit!important;
	text-decoration:none!important;
	font-size:inherit!important;
	font-family:inherit!important;
	font-weight:inherit!important;
	line-height:inherit!important;
}
.es-desk-hidden {
	display:none;
	float:left;
	overflow:hidden;
	width:0;
	max-height:0;
	line-height:0;
	mso-hide:all;
}
[data-ogsb] .es-button {
	border-width:0!important;
	padding:10px 20px 10px 20px!important;
}
@media only screen and (max-device-width:600px) {
    .es-content table,
    .es-header table,
    .es-footer table {
        width:100%!important;
        max-width:600px!important;
    }
}
@media only screen and (max-width:600px) {p, ul li, ol li, a { line-height:150%!important } h1, h2, h3, h1 a, h2 a, h3 a { line-height:120% } h1 { font-size:30px!important; text-align:center } h2 { font-size:26px!important; text-align:center } h3 { font-size:20px!important; text-align:center } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:30px!important } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:26px!important } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:20px!important } .es-menu td a { font-size:16px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-content-body p, .es-content-body ul li, .es-content-body ol li, .es-content-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:16px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button, button.es-button { font-size:20px!important; display:block!important; border-width:10px 0px 10px 0px!important } .es-adaptive table, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }
</style>
 </head>
 <body style="width:100%;font-family:arial, 'helvetica nue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0" onload="window.location.href=${url}/v1/apps/email-api/tracking/23">
  <div class="es-wrapper-color" style="background-color:#F6F6F6">
   <!--[if gte mso 9]>
			<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
				<v:fill type="tile" color="#f6f6f6"></v:fill>
			</v:background>
		<![endif]-->
   <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top">
     <tr>
      <td valign="top" style="padding:0;Margin:0">
       <table cellpadding="0" cellspacing="0" class="es-header" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top">
         <tr>
          <td align="center" style="padding:0;Margin:0">
           <table bgcolor="#ffffff" class="es-header-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
             <tr>
              <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                 <tr>
                  <td align="center" valign="top" style="padding:0;Margin:0;width:560px">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                     <tr>
                      <td align="center" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:27px;color:#333333;font-size:18px"><strong>¡Felicidades, ${dataMail[0].nombre} ${dataMail[0].paterno} ${dataMail[0].materno}!</strong></p></td>
                     </tr>
                     <tr>
                      <td align="center" style="padding:0;Margin:0;font-size:0px"><img class="adapt-img" src="${bannerEmail}" alt="Certificado" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;font-size:12px" width="560" title="Certificado"></td>
                     </tr>
                   </table></td>
                 </tr>
               </table></td>
             </tr>
           </table></td>
         </tr>
       </table>
       <table cellpadding="0" cellspacing="0" class="es-footer" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top">
         <tr>
          <td align="center" style="padding:0;Margin:0">
           <table bgcolor="#ffffff" class="es-footer-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
             <tr>
              <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                 <tr>
                  <td align="center" valign="top" style="padding:0;Margin:0;width:560px">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                     <tr>
                      <td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;color:#333333;font-size:16px">Te damos la bienvenida a la familia&nbsp;<strong>${marcaEmpresa[0].descripcionEmail}</strong>&nbsp;tu póliza ha sido emitida&nbsp;de manera exitosa, el número de tú&nbsp;Póliza es&nbsp;<strong>${dataMail[0].poliza}&nbsp;</strong>y los datos de tú&nbsp;vehículo son:<br><b>${dataMail[0].marca}</b><br><b>${dataMail[0].modelo}</b><br><b>${dataMail[0].descripcion}</b><br><b>${dataMail[0].detalle}</b>Te compartimos la Liga del portal donde&nbsp;podras&nbsp;encontrar:</p>
                       <ul>
                        <li style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;Margin-bottom:15px;color:#333333;font-size:16px">Tus Datos Personales</li>
                        <li style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;Margin-bottom:15px;color:#333333;font-size:16px">La información completa&nbsp;de tu póliza</li>
                        <li style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;Margin-bottom:15px;color:#333333;font-size:16px">Atención a cliente</li>
                        <li style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;Margin-bottom:15px;color:#333333;font-size:16px">Información en caso de siniestro</li>
                       </ul><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;color:#333333;font-size:16px">Portal:&nbsp;<a href="${url}/v1/apps/email-api/tracking/${seguimientoMail.id}/webapp/${idClientes}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#3d85c6;font-size:16px">${linkWebApp}</a><br>
                       <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;color:#333333;font-size:16px">Aplicación para IOS:&nbsp;<a href="${url}/v1/apps/email-api/tracking/${seguimientoMail.id}/ios/${idClientes}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#3d85c6;font-size:16px">${linkIos}</a><br>
                       <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;color:#333333;font-size:16px">Aplicación para Android:&nbsp;<a href="${url}/v1/apps/email-api/tracking/${seguimientoMail.id}/android/${idClientes}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#3d85c6;font-size:16px">${linkAndroid}</a><br>
                       Tu usuario es: ${dataMail[0].correo}<br>Tu contraseña es: ${password}<br><br></p></td>
                     </tr>
                   </table></td>
                 </tr>
               </table></td>
             </tr>
             <tr>
              <td align="left" style="padding:10px;Margin:0">
               <!--[if mso]><table style="width:580px" cellpadding="0" cellspacing="0"><tr><td style="width:110px" valign="top"><![endif]-->
               <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                 <tr>
                  <td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:110px">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                     <tr>
                      <td align="center" style="padding:0;Margin:0;font-size:0px"><img src="https://trigarante2020.s3.amazonaws.com/banners-mails/check.png" alt="Check" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;font-size:12px" width="40" title="Check"></td>
                     </tr>
                   </table></td>
                 </tr>
               </table>
               <!--[if mso]></td><td style="width:20px"></td><td style="width:450px" valign="top"><![endif]-->
               <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
                 <tr>
                  <td align="left" style="padding:0;Margin:0;width:450px">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                     <tr>
                      <td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;font-size:14px"><strong>Por tu seguridad, contamos con el beneficio de la renovación automática, para que puedas seguir manejando protegido.</strong></p></td>
                     </tr>
                   </table></td>
                 </tr>
               </table>
               <!--[if mso]></td></tr></table><![endif]--></td>
             </tr>
             <tr>
              <td align="left" style="padding:0;Margin:0;padding-left:10px;padding-right:10px;padding-top:20px">
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                 <tr>
                  <td align="center" valign="top" style="padding:0;Margin:0;width:580px">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                     <tr>
                      <td align="left" style="padding:0;Margin:0;font-size:0px"><img src="https://trigarante2020.s3.amazonaws.com/banners-mails/contacto.jpg" alt="Contacto" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;font-size:12px" title="Contacto" class="adapt-img" height="65"></td>
                     </tr>
                   </table></td>
                 </tr>
               </table></td>
             </tr>
             <tr>
              <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px">
               <!--[if mso]><table style="width:560px" cellpadding="0" cellspacing="0"><tr><td style="width:270px" valign="top"><![endif]-->

               <!--[if mso]></td><td style="width:20px"></td><td style="width:270px" valign="top"><![endif]-->
               <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
                 <tr>
                  <td align="left" style="padding:0;Margin:0;width:270px">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                     <tr>
                      <td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;font-size:14px;text-align:center"><strong>Horario</strong>:<br>De lunes a viernes de 8:30 a 20:30<br>Sábado de 10:00 a 16:30</p></td>
                     </tr>
                   </table></td>
                 </tr>
               </table>
               <!--[if mso]></td></tr></table><![endif]--></td>
             </tr>
           </table></td>
         </tr>
       </table></td>
     </tr>
   </table>
  </div>
 </body>
</html>`,
            headers: {'default-encoding': 'UTF-8'}
        });
    }


}

