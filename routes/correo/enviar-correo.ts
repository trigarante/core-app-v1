import {Request, Response, Router} from "express";
import https from "https";
// import fetch from 'node-fetch';
import axios from "axios";

const Correo = Router();

const token = '711e3debb2f09b743cd0b1664c26bee0bd2023d7:9e4ea5660a2112bd4ee724b1e1bf7cde7addda20'; // user:password
const tokenBase64 = Buffer.from(token).toString('base64'); // token convertido en base 64

axios.defaults.headers.common.Authorization = 'Basic ' + tokenBase64; // se agrega el header en su propiedad Authorization

Correo.get('/correos', async (req: Request, res: Response) => {
    axios.get('https://apiv3.emailsys.net/v1/mailings')
        .then(data => res.status(200).send(data.data))
        .catch(err => console.error(err))
});
Correo.post('/create-recipientList', async (req: Request, res: Response) => {

    const body = {
        name: "List from insomnia",
        description: "A recipient list test",
        unsubscribe_blacklist: "no",
        recipient_subscribe_email: "no",
        default: "no"
    };
    axios.post('https://apiv3.emailsys.net/v1/recipientlists', body)
        .then(data => res.status(200).send(data.data))
        .catch(err => console.error(err))

    /*
    * JSON QUE REGRESA CUANDO SE CREA UN RECIPIENTLIST
    * {
  "id": 20851,
  "name": "List from insomnia",
  "description": "A recipient list test",
  "unsubscribe_blacklist": "no",
  "default": "no",
  "recipient_subscribe_email": "no",
  "created": "2021-09-06 23:44:48",
  "updated": "2021-09-06 23:44:48",
  "subscribe_form_url": "https://t71fb4744.emailsys4a.net/38/20851/214135edea/subscribe/form.html",
  "subscribe_form_field_key": "rm_email",
  "_links": {
    "self": {
      "href": "https://apiv3.emailsys.net/v1/recipientlists/20851"
    }
  }
}
    * */
});
Correo.post('/send-mailing', async (req: Request, res: Response) => {

    const body = {
        status: "draft",
        destinations: [
            {
                type: "recipientlist",
                id: "20883",
                action: "include"
            }
        ],
        from_name: "Michael J. Fox",
        from_email: "sreyes@trigarante.com",
        subject: "My example mailing",
        check_ecg: "no",
        check_robinson: "no",
        file: {
            content: "string",
            type: "application/zip"
        }
    };
    axios.post('https://apiv3.emailsys.net/v1/recipientlists', body)
        .then(data => res.status(200).send(data.data))
        .catch(err => res.status(500).send(err))
});
Correo.put('/update-recipientList/:recipientlist_id', async (req: Request, res: Response) => {

    const id = req.params.recipientlist_id;

    const body = {
        name: "List from insomnia update",
        description: "A recipient list test",
        unsubscribe_blacklist: "no",
        recipient_subscribe_email: "no",
        default: "no"
    };

    axios.patch(`https://apiv3.emailsys.net/v1/recipientlists/${id}`, body)
        .then(data => res.status(200).send(data.data))
        .catch(err => res.status(500).send(err))
});

export default Correo;
