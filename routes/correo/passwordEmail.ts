import * as fs from "fs";
import {google} from "googleapis";
import * as readline from "readline";
import CanalWhatsappService from "../../services/CanalWhatsappService";
import MarcasEmpresaService from "../../services/MarcasEmpresaService";
import SeguimientoMailService from "../../services/SeguimientoMailService";
import ClienteService from "../../services/ClienteService";

export default class PasswordEmail {

    private static loadClienteSecret(idEmpresa): Promise<any> {
        return new Promise((resolve, reject) => {
            switch (idEmpresa) {
                case 1:
                    fs.readFile('config/credentials.json', (err, content) => {
                        if (err) reject(console.log('Error loading client secret file:', err));
                        // Authorize a client with credentials, then call the Google drive API.
                        resolve(JSON.parse(content.toString()).installed);
                    })
                    break;
                case 3:
                    fs.readFile('config/credentials-mejor-seguro.json', (err, content) => {
                        if (err) reject(console.log('Error loading client secret file:', err));
                        // Authorize a client with credentials, then call the Google drive API.
                        resolve(JSON.parse(content.toString()).installed);
                    })
                    break;
                case 5:
                    fs.readFile('config/credentials-migo.json', (err, content) => {
                        if (err) reject(console.log('Error loading client secret file:', err));
                        // Authorize a client with credentials, then call the Google drive API.
                        resolve(JSON.parse(content.toString()).installed);
                    })
                    break;
            }
        });
    }

    private static async authorize(scope, idMarcaEmpresa): Promise<any> {
        const {client_secret, client_id, redirect_uris} = await this.loadClienteSecret(idMarcaEmpresa);
        const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
        let scopes;
        let path = 'config/';

        switch (idMarcaEmpresa) {
            case 1:
                path += 'token.json';
                scopes = ['https://mail.google.com/',
                    'https://www.googleapis.com/auth/gmail.modify',
                    'https://www.googleapis.com/auth/gmail.compose',
                    'https://www.googleapis.com/auth/gmail.send'];
                break;
            case 2:
                path += 'tokenSend.json';
                scopes = ['https://www.googleapis.com/auth/gmail.send'];
                break;
            case 3:
                path += 'token-mejor-seguro.json';
                scopes = ['https://mail.google.com/',
                    'https://www.googleapis.com/auth/gmail.modify',
                    'https://www.googleapis.com/auth/gmail.compose',
                    'https://www.googleapis.com/auth/gmail.send'];
                break;
            case 4:
                path += 'tokenCompose.json';
                scopes = ['https://www.googleapis.com/auth/gmail.compose'];
                break;
            case 5:
                path += 'token-migo.json';
                scopes = ['https://mail.google.com/',
                    'https://www.googleapis.com/auth/gmail.modify',
                    'https://www.googleapis.com/auth/gmail.compose',
                    'https://www.googleapis.com/auth/gmail.send'];
                break;
        }

        return new Promise((resolve) => {
            // Check if we have previously stored a token.
            fs.readFile(path, (err, token) => {
                if (err) {
                    resolve(PasswordEmail.getAccessToken(oAuth2Client, scopes, path, idMarcaEmpresa));
                } else {
                    oAuth2Client.setCredentials(JSON.parse(token.toString()));
                    resolve(oAuth2Client);
                }
            });
        });
    }

    private static async getAccessToken(oAuth2Client, scopes, path, idMarcaEmpresa): Promise<any> {
        return new Promise(resolve => {
            const authUrl = oAuth2Client.generateAuthUrl({
                access_type: 'offline',
                scope: scopes,
            });
            console.log('Authorize this app by visiting this url:', authUrl);
            const rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout,
            });
            rl.question('Enter the code from that page here: ', (code) => {
                rl.close();
                oAuth2Client.getToken(code, (err, token) => {
                    if (err) console.error('Error retrieving access token', err);
                    oAuth2Client.setCredentials(token);
                    fs.writeFile('config/token-migo.json', JSON.stringify(token), (error) => {
                        if (error) console.error(error);
                        console.log('Token stored to', path);
                        resolve(oAuth2Client);
                    });
                });
            });
        });
    }


    static async sendPasswordEmail(idClienteApp, idMarca) {
        const auth = await PasswordEmail.authorize(5, idMarca);
        const marcaEmpresa = await MarcasEmpresaService.getById(idMarca);
        const linkWebApp = marcaEmpresa.linkWebApp;
        const linkAndroid = marcaEmpresa.linkAndroid;
        const linkIos = marcaEmpresa.linkIos;
        const bannerEmail = marcaEmpresa.bannerEmail;
        const cliente = await ClienteService.getById(idClienteApp);

        const dataRecovery = {
            idCliente: idClienteApp,
            idMarcaEmpresa: idMarca,
            activo: 1,
        };
        const passRecovery = await SeguimientoMailService.recoveryPassword(dataRecovery);
        const enlace = `${'jjj'}/#/forgot-password/${Buffer.from('' + passRecovery.id).toString('base64')}`;
        const dataUpdateRecovery = {
            url: enlace
        };
        await SeguimientoMailService.updateRecoveryPassword(dataUpdateRecovery, passRecovery.id)
        return new Promise(resolve => {
            const gmail = google.gmail({version: 'v1', auth});
            const subject = `Bienvenido a ${marcaEmpresa.descripcionEmail}`;
            const utf8Subject = `=?utf-8?B?${Buffer.from(subject).toString('base64')}?=`;

            const messageParts = [
                `From: ${marcaEmpresa.descripcionEmail} ${marcaEmpresa.emailSalida}`,
                `To: ${cliente.nombre} <${cliente.correo}>'`,
                'Content-Type: text/html; charset=utf-8',
                'MIME-Version: 1.0',
                `Subject: ${utf8Subject}`,
                '',
                `Dirigete al siguiente link para recuperar tu contraseña: ${enlace}\n`
            ];
            const message = messageParts.join('\n');

            const encodedMessage = Buffer.from(message)
                .toString('base64')
                .replace(/\+/g, '-')
                .replace(/\//g, '_')
                .replace(/=+$/, '');
            gmail.users.messages.send({
                userId: 'me',
                requestBody: {
                    raw: encodedMessage,
                },
            }, (err, res) => {
                if (err) return console.log('The API returned an error: ' + err);
                return res.data;
            });
        });
    }
}
