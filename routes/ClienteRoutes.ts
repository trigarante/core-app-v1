import {Request, Response, Router} from "express";
import ClienteService from "../services/ClienteService";
import archivosBase64 from "../general/archivosBase64";
import axios from "axios";
import AppsService from "../services/AppsService";

const general = require('../general/function');

const router = Router();

router.post('/login', async (req: Request, res: Response, next) => {
    try {
        const {contrasenaApp, email, idMarcaEmpresa} = req.body;
        if (!(contrasenaApp && email)) return general.manejoErrores('No se envio la contraseña o el email', res);
        const cliente = await ClienteService.getByCorreoAndContrasenaApp(email, contrasenaApp);
        if (!cliente) return general.manejoErrores('No existe el cliente', res);
        const tipo = cliente.acceso === 1 ? 1 : 0;
        await logueoApp(cliente.id, idMarcaEmpresa);
        return res.status(200).json({
            id: cliente.id,
            tipo,
            msg: 'usuario existente',
        })
    } catch (err) {
        next(err);
    }

    async function logueoApp(idCliente, idMarcaEmpresa) {
        const data = {
            idCliente,
            idMarcaEmpresa
        };

        return await AppsService.createLogueoApp(data);
    }
});

router.put('/login/:idCliente', async (req: Request, res: Response, next) => {
    try {
        const cliente = await ClienteService.getById(req.params.idCliente);
        await ClienteService.updatePasswordFront(req.params.idCliente, req.body)
        return res.status(200);
    } catch (err) {
        next(err);
    }
});

router.get('/:id', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        const cliente = await ClienteService.getById(+id);
        res.status(200).send(cliente);
    } catch (err) {
        next(err);
    }
});

router.get('/poliza/:base64', async (req: Request, res: Response, next) => {
    try {
        const archivo = req.params.base64;
        const {idcliente, idregistro, idmarca} = req.headers;
        if (archivo === 'aviso-privacidad') {
            await descargaPoliza();
            return res.end(archivosBase64.pdfBase64, "base64");
        } else {
            const url = `https://documentos.mark-43.net/mark43-service/v1/api-drive/obtener-documentos/2/${archivo}`;
            axios.get(url)
                .then(async body => {
                    const pdfBase64: string = body.data[0];
                    res.writeHead(200, {
                        'Content-Type': 'application/pdf',
                        'Content-Disposition': 'attachment; filename=AhorraSeguros-Poliza.pdf'
                    });
                    await descargaPoliza();
                    return res.end(pdfBase64, "base64");
                })
                .catch(err => {
                    console.log('Hubo un error: ', err);
                    return general.manejoErrores('Hubo un errpr al leer el pdf');
                })
        }

        async function descargaPoliza() {
            const data = {
                idCliente: idcliente,
                idRegistro: idregistro,
                idMarcaEmpresa: idmarca
            };

            return await AppsService.downloadPoliza(data);
        }
    } catch (err) {
        next(err);
    }
});

router.get('/poliza-view/:base64', async (req: Request, res: Response, next) => {
    try {
        const archivo = req.params.base64;
        const {idcliente, idregistro, idmarca} = req.headers;
        if (archivo === 'aviso-privacidad') {
            return res.end(archivosBase64.pdfBase64, "base64");
        } else {
            const url = `https://documentos.mark-43.net/mark43-service/v1/api-drive/obtener-documentos/2/${archivo}`;
            axios.get(url)
                .then(async body => {
                    const pdfBase64: string = body.data[0];
                    res.writeHead(200, {
                        'Content-Type': 'application/pdf',
                        'Content-Disposition': 'attachment; filename=AhorraSeguros-Poliza.pdf'
                    });
                    return res.end(pdfBase64, "base64");
                })
                .catch(err => {
                    console.log('Hubo un error: ', err);
                    return general.manejoErrores('Hubo un errpr al leer el pdf');
                })
        }
    } catch (err) {
        next(err);
    }
});

router.get('/get-registro-cliente/:idCliente', async (req: Request, res: Response, next) => {
    try {
        const {idCliente} = req.params;
        const polizas = await ClienteService.getAllPolizasByIdCliente(+idCliente);
        res.status(200).send(polizas);
    } catch (err) {
        next(err);
    }
});

router.get('/get-by-email/:email', async (req: Request, res: Response, next) => {
    try {
        const email = req.params.email;
        const usuario = await ClienteService.getByEmail(email);
        res.status(200).send(usuario);
    } catch (err) {
        next(err);
    }
});
export default router;
