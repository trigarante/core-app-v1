import db from "../db/connection";
import {Model, DataTypes} from "sequelize";

const sequelize = db;

class WebhookMailchimp extends Model {
    id!: number;
    respuesta!: string;
}

WebhookMailchimp.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    respuesta: {
        type: DataTypes.JSON
    }
}, {
    timestamps: false,
    tableName: 'webhookMailchimp',
    sequelize
});

export default WebhookMailchimp;
