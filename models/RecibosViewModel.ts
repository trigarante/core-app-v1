import db from "../db/connection";
import {Model, DataTypes} from "sequelize";
const sequelize = db;

class RecibosView extends Model {}

RecibosView.init({
    id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        primaryKey: true,
    },
    idRegistro: {
        type: DataTypes.BIGINT
    },
    idEmpleado: {
        type: DataTypes.BIGINT
    },
    idEstadoRecibos: {
        type: DataTypes.INTEGER
    },
    numero: {
        type: DataTypes.INTEGER
    },
    cantidad: {
        type: DataTypes.FLOAT
    },
    fechaVigencia: {
        type: DataTypes.DATE
    },
    fechaLiquidacion: {
        type: DataTypes.DATE
    },
    estdo: {
        type: DataTypes.STRING
    },
    idEstadoPoliza: {
        type: DataTypes.INTEGER
    },
    primaNeta: {
        type: DataTypes.FLOAT
    },
    poliza: {
        type: DataTypes.STRING
    },
    estado: {
        type: DataTypes.STRING
    },
    idArea: {
        type: DataTypes.INTEGER
    },
    idPuesto: {
        type: DataTypes.INTEGER
    },
    idEmpresa: {
        type: DataTypes.INTEGER
    },
    idGrupo: {
        type: DataTypes.INTEGER
    },
    nombre: {
        type: DataTypes.STRING
    },
    apellidoPaterno: {
        type: DataTypes.STRING
    },
    apellidoMaterno: {
        type: DataTypes.STRING
    },
    idSubarea: {
        type: DataTypes.INTEGER
    },
    idSede: {
        type: DataTypes.INTEGER
    },
    idProductoSocio: {
        type: DataTypes.INTEGER
    },
    nombreComercial: {
        type: DataTypes.STRING
    },
    area: {
        type: DataTypes.STRING
    },
    fechaRegistro: {
        type: DataTypes.DATE
    },
    idFlujoPoliza: {
        type: DataTypes.INTEGER
    },
    fechaInicio: {
        type: DataTypes.DATE
    },
    fechaPromesaPago: {
        type: DataTypes.DATE
    },
    datos: {
        type: DataTypes.STRING
    },
    fechaPago: {
        type: DataTypes.DATE
    },
    idPago: {
        type: DataTypes.BIGINT
    },
    idFormaPago: {
        type: DataTypes.INTEGER
    },
    nombreEmpleadoPago: {
        type: DataTypes.STRING
    },
    apellidoPaternoEmpleadoPago: {
        type: DataTypes.STRING
    },
    apellidoMaternoEmpleadoPago: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.INTEGER
    },
    archivo: {
        type: DataTypes.STRING
    },
}, {
    timestamps: false,
    tableName: 'recibosView',
    sequelize
});

export default RecibosView;