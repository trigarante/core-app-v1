import db from "../db/connection";
import {Model, DataTypes} from "sequelize";

const sequelize = db;

class ClienteModel extends Model {
    id!: number;
    idPais!: number;
    razonSocial!: number;
    nombre!: string;
    paterno!: string;
    materno!: string;
    cp!: number;
    calle!: string;
    numInt!: string;
    numExt!: string;
    idColonia!: number;
    genero!: string;
    telefonoFijo!: string;
    telefonoMovil!: string;
    correo!: string;
    fechaNacimiento!: Date;
    curp!: string;
    rfc!: string;
    archivo!: string;
    archivoSubido!: number;
    ruc!: string;
    dni!: string;
    idColoniaPeru!: number;
    contrasenaApp!: string;
    token!: string;
    acceso!: number;
}

ClienteModel.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    idPais: {
        type: DataTypes.INTEGER,
    },
    razonSocial: {
        type: DataTypes.INTEGER,
    },
    nombre: {
        type: DataTypes.STRING,
    },
    paterno: {
        type: DataTypes.STRING,
    },
    materno: {
        type: DataTypes.STRING,
    },
    cp: {
        type: DataTypes.INTEGER,
    },
    calle: {
        type: DataTypes.STRING,
    },
    numInt: {
        type: DataTypes.STRING,
    },
    numExt: {
        type: DataTypes.STRING,
    },
    idColonia: {
        type: DataTypes.INTEGER,
    },
    genero: {
        type: DataTypes.STRING,
    },
    telefonoFijo: {
        type: DataTypes.STRING,
    },
    telefonoMovil: {
        type: DataTypes.STRING,
    },
    correo: {
        type: DataTypes.STRING,
    },
    fechaNacimiento: {
        type: DataTypes.DATE,
    },
    curp: {
        type: DataTypes.STRING,
    },
    rfc: {
        type: DataTypes.STRING,
    },
    archivo: {
        type: DataTypes.STRING,
    },
    archivoSubido: {
        type: DataTypes.INTEGER,
    },
    ruc: {
        type: DataTypes.STRING,
    },
    dni: {
        type: DataTypes.STRING,
    },
    idColoniaPeru: {
        type: DataTypes.INTEGER,
    },
    contrasenaApp: {
        type: DataTypes.STRING,
    },
    token: {
        type: DataTypes.STRING,
    },
    acceso: {
        type: DataTypes.INTEGER,
    },
}, {
    tableName: 'cliente',
    timestamps: false,
    sequelize,
});

export default ClienteModel;
