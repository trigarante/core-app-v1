import db from "../db/connection";
import {Model, DataTypes} from "sequelize";

const sequelize = db;

class CanalWhatsapp extends Model {
    id!: number;
    idSolicitud!: number;
    idMarcasEmpresa!: number;
}

CanalWhatsapp.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    idSolicitud: {
        type: DataTypes.NUMBER
    },
    idMarcasEmpresa: {
        type: DataTypes.NUMBER
    },
}, {
    timestamps: false,
    tableName: 'canalWhatsapp',
    sequelize
});

export default CanalWhatsapp;
