import db from "../db/connection";
import {Model, DataTypes} from "sequelize";

const sequelize = db;

class MarcasEmpresa extends Model {
    id!: number;
    descripcion!: string;
    activo!: number;
    imagenPlantilla!: string;
    linkWebApp!: string;
    linkAndroid!: string;
    linkIos!: string;
    bannerEmail!: string;
    descripcionEmail!: string;
    emailSalida!: string;
}

MarcasEmpresa.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    descripcion: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.TINYINT
    },
    imagenPlantilla: {
        type: DataTypes.STRING
    },
    linkWebApp: {
        type: DataTypes.STRING
    },
    linkAndroid: {
        type: DataTypes.STRING
    },
    linkIos: {
        type: DataTypes.STRING
    },
    bannerEmail: {
        type: DataTypes.STRING
    },
    descripcionEmail: {
        type: DataTypes.STRING
    },
    emailSalida: {
        type: DataTypes.STRING
    },
}, {
    timestamps: false,
    tableName: 'marcasEmpresa',
    sequelize
});

export default MarcasEmpresa;
