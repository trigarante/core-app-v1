import db from "../db/connection";
import {DataTypes, Model} from "sequelize";

const sequelize = db;

class PasswordRecovery extends Model {
    id!: number;
    idCliente!: number;
    idMarcaEmpresa!: number;
    url!: string;
    activo!: number;
    fechaRegistro: Date;
}

PasswordRecovery.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },

    idCliente: {
        type: DataTypes.NUMBER
    },

    idMarcaEmpresa: {
        type: DataTypes.NUMBER
    },

    url: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.TINYINT
    },
    fechaRegistro: {
        type: DataTypes.DATE
    }
}, {
    timestamps: false,
    tableName: 'passwordRecovery',
    sequelize
});

export default PasswordRecovery;
