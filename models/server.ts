import express, { Application } from 'express';
import cors from 'cors';
import db from '../db/connection';
import routes from "../routes";

const io = require('socket.io');


class Server {

    private app: Application;
    private port: string;

    private server;

    constructor() {
        this.app = express();
        this.port = process.env.PORT || '8000';

        // Métodos iniciales
        this.dbConnection();
        this.middlewares();
        this.socketInit();
        this.routes();
    }

    async dbConnection() {
        try {
            await db.authenticate();
            console.log('Database online');
        } catch (error) {
            throw new Error(error);
        }

    }

    middlewares() {
        // CORS
        this.app.use(cors());

        // Lectura del body
        this.app.use(express.json());

        // Carpeta pública
        this.app.use(express.static('public'));
    }


    routes() {
        this.app.use("/", routes)
    }


    listen() {
        this.server = this.app.listen(this.port, () => {
            console.log('Servidor corriendo en puerto ' + this.port);
        })
    }

    socketInit() {
        const socketIO = io(this.server, {
            cors: {
                origin: "http://localhost:4500",
                methods: ["GET", "POST"],
                allowedHeaders: ["socket-cors"],
                credentials: true
            }
        });

        socketIO.sockets.on('connection', (socket) => {
            console.log('usuario conectado => ', socket.id);

            /* socket.on('dataSocket', (data) => {
                socket.broadcast.to(`ticket_${data.ticketid}`).emit('broadcast', {
                    message: data.message
                });
            }); */
        })


    }

}

export default Server;
