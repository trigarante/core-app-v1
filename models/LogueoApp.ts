import db from "../db/connection";
import {Model, DataTypes} from "sequelize";

const sequelize = db;

class LogueoApp extends Model {
    id!: number;
    idCliente!: number;
    fechaInicio!: Date;
    fechaCierre!: Date;
    idMarcaEmpresa!: number;
}

LogueoApp.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    idCliente: {
        type: DataTypes.INTEGER,
    },
    fechaInicio: {
        type: DataTypes.DATE,
    },
    fechaCierre: {
        type: DataTypes.DATE,
    },
    idMarcaEmpresa: {
        type: DataTypes.INTEGER,
    }
}, {
    tableName: 'logueoApp',
    timestamps: false,
    sequelize,
});

export default LogueoApp;
