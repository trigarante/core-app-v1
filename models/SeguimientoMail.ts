import db from "../db/connection";
import {Model, DataTypes} from "sequelize";

const sequelize = db;

class SeguimientoMail extends Model {
    id!: number;
    idCliente!: number;
    idMail!: string;
    enviado!: number;
    recibido!: number;
    abierto!: number;
    clickEnlaceWebApp!: number;
    clickEnlaceIos!: number;
    clickEnlaceAndroid!: number;
    idThread!: string;
    idMarcaEmpresa!: number;
    idRegistro!: number;
}

SeguimientoMail.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    idCliente: {
        type: DataTypes.NUMBER
    },
    idMail: {
        type: DataTypes.STRING
    },
    enviado: {
        type: DataTypes.TINYINT
    },
    recibido: {
        type: DataTypes.TINYINT
    },
    abierto: {
        type: DataTypes.TINYINT
    },
    clickEnlaceWebApp: {
        type: DataTypes.TINYINT
    },
    clickEnlaceIos: {
        type: DataTypes.TINYINT
    },
    clickEnlaceAndroid: {
        type: DataTypes.TINYINT
    },
    idThread: {
        type: DataTypes.STRING
    },
    idMarcaEmpresa: {
        type: DataTypes.INTEGER
    },
    idRegistro: {
        type: DataTypes.INTEGER
    },
}, {
    timestamps: false,
    tableName: 'seguimientoMail',
    sequelize
});
export default SeguimientoMail;
