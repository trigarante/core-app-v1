import db from "../db/connection";
import {Model, DataTypes} from "sequelize";

const sequelize = db;

class DescargaPoliza extends Model {
    id!: number;
    idCliente!: number;
    idRegistro!: number;
    fechaDescarga!: Date;
    idMarcaEmpresa!: number;
}

DescargaPoliza.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    idCliente: {
        type: DataTypes.INTEGER,
    },
    idRegistro: {
        type: DataTypes.INTEGER,
    },
    fechaDescarga: {
        type: DataTypes.DATE,
    },
    idMarcaEmpresa: {
        type: DataTypes.INTEGER,
    }
}, {
    tableName: 'descargaPoliza',
    timestamps: false,
    sequelize,
});

export default DescargaPoliza;
