import CanalWhatsapp from "../models/CanalWhatsapp";
import db from "../db/connection";

export default class CanalWhatsappService {
    static async getById(id: any) {
        return (await db.query(`SELECT
canalWhatsapp.*
FROM
registro
INNER JOIN productoCliente ON registro.idProducto = productoCliente.id
INNER JOIN canalWhatsapp ON canalWhatsapp.idSolicitud = productoCliente.idSolictud
WHERE registro.id = ${id}`))[0];
    }
}
