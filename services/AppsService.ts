import db from "../db/connection";
import {QueryTypes} from "sequelize";
import LogueoApp from "../models/LogueoApp";
import DescargaPoliza from "../models/DescargaPoliza";

export default class AppsService {
    static async createLogueoApp(data) {
        return await LogueoApp.create({...data});
    }

    static async updateCliente(data) {
        return await LogueoApp.create({...data});
    }

    static async downloadPoliza(data) {
        return await DescargaPoliza.create({...data});
    }

    static async getAllLogueoApp() {
        return await db.query(`
            SELECT logueoApp.id,
                   logueoApp.idCliente,
                   logueoApp.fechaInicio,
                   logueoApp.fechaCierre,
                   logueoApp.idMarcaEmpresa,
                   cliente.nombre,
                   cliente.paterno,
                   cliente.materno,
                   cliente.token,
                   cliente.contrasenaApp,
                   cliente.acceso,
                   marcasEmpresa.descripcion
            FROM logueoApp
                     INNER JOIN cliente ON cliente.id = logueoApp.idCliente
                     LEFT JOIN marcasEmpresa ON marcasEmpresa.id = logueoApp.idMarcaEmpresa
        `, {
            type: QueryTypes.SELECT,
        })
    }

    static async getAllDescargasPoliza() {
        return await db.query(`
            SELECT descargaPoliza.id,
                   descargaPoliza.idRegistro,
                   descargaPoliza.idCliente,
                   descargaPoliza.fechaDescarga,
                   cliente.nombre,
                   cliente.paterno,
                   cliente.materno,
                   cliente.token,
                   cliente.corregido,
                   cliente.contrasenaApp,
                   registro.poliza,
                   registro.primaNeta,
                   registro.fechaInicio,
                   registro.fechaRegistro,
                   registro.archivo,
                   registro.fechaFin,
                   registro.fechaCobranza,
                   marcasEmpresa.descripcion as marca
            FROM descargaPoliza
                     INNER JOIN cliente ON cliente.id = descargaPoliza.idCliente
                     INNER JOIN registro ON registro.id = descargaPoliza.idRegistro
                     LEFT JOIN marcasEmpresa ON marcasEmpresa.id = descargaPoliza.idMarcaEmpresa
        `, {
            type: QueryTypes.SELECT,
        })
    }

    static async getMailSeguimiento() {
        return await db.query(`
            SELECT cliente.nombre,
                   cliente.paterno,
                   cliente.materno,
                   seguimientoMail.id,
                   seguimientoMail.idCliente,
                   seguimientoMail.idMail,
                   seguimientoMail.enviado,
                   seguimientoMail.recibido,
                   seguimientoMail.abierto,
                   seguimientoMail.clickEnlaceWebApp,
                   seguimientoMail.idThread,
                   seguimientoMail.clickEnlaceIos,
                   seguimientoMail.clickEnlaceAndroid,
                   seguimientoMail.fechaEnvio as fechaEnvio
            FROM cliente
                     INNER JOIN seguimientoMail ON cliente.id = seguimientoMail.idCliente
        `, {
            type: QueryTypes.SELECT,
        })
    }

    static async getReporteGlobal() {
        return await db.query(`
            SELECT (SELECT count(id) FROM descargaPoliza)  AS descargasPolizas,
                   (SELECT count(id) FROM logueoApp)       AS logueo,
                   (SELECT count(id) FROM seguimientoMail) AS mailEnviados
        `, {
            type: QueryTypes.SELECT,
        })
    }

    static async getReporteByIdEmpresa(id) {
        return await db.query(`
            SELECT (SELECT count(id) FROM descargaPoliza WHERE idMarcaEmpresa = ${id})  AS descargasPolizas,
                   (SELECT count(id) FROM logueoApp WHERE idMarcaEmpresa = ${id})       AS logueo,
                   (SELECT count(id) FROM seguimientoMail WHERE idMarcaEmpresa = ${id}) AS mailEnviados
        `, {
            type: QueryTypes.SELECT,
        })
    }

    static async getEmpresas() {
        return await db.query(`
            SELECT *
            from marcasEmpresa
        `, {
            type: QueryTypes.SELECT,
        })
    }
}
