import MarcasEmpresa from "../models/MarcasEmpresa";
import db from "../db/connection";

export default class MarcasEmpresaService {
    static async getByIdRegistro(id: any) {
        return (await db.query(`
            SELECT marcasEmpresa.*,
                   solicitudes.id   as idSolicitud,
                   marcasEmpresa.id as idMarcasEmpresa
            FROM registro
                     LEFT JOIN
                 productoCliente
                 ON
                     registro.idProducto = productoCliente.id
                     LEFT JOIN
                 solicitudes
                 ON
                     productoCliente.idSolictud = solicitudes.id
                     LEFT JOIN
                 cotizacionesAli
                 ON
                     solicitudes.idCotizacionAli = cotizacionesAli.id
                     LEFT JOIN
                 cotizaciones
                 ON
                     cotizacionesAli.idCotizacion = cotizaciones.id
                     LEFT JOIN
                 medioDifusion
                 ON
                     cotizaciones.idMedioDifusion = medioDifusion.id
                     LEFT JOIN
                 marcasEmpresa
                 ON
                     medioDifusion.idMarcaEmpresa = marcasEmpresa.id
            WHERE registro.id = ${id}`))[0];
    }

    static async getById(idEmpresa: number) {
        return await MarcasEmpresa.findOne({
            where: {
                id: idEmpresa
            }
        })
    }
}
