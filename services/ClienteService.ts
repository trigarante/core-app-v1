import ClienteModel from "../models/ClienteModel";
import db from "../db/connection";
import {QueryTypes} from "sequelize";

export default class ClienteService {
    static async getByCorreoAndContrasenaApp(correo: string, contrasenaApp: string) {
        return await ClienteModel.findOne({
            where: {correo, contrasenaApp}
        })
    }

    static async getById(id: any) {
        return await ClienteModel.findOne({
            where: {
                id,
            }
        })
    }

    static async getByEmail(email: string) {
        return await ClienteModel.findOne({
            where: {
                correo: email,
            }
        })
    }

    static async updatePassword(idCliente, passwordNew) {
        return await ClienteModel.update({contrasenaApp: passwordNew}, {
            where: {
                id: idCliente
            }
        })
    }

    static async updatePasswordFront(idCliente, body) {
        return await ClienteModel.update({contrasenaApp: body.contrasenaApp}, {
            where: {
                id: idCliente
            }
        })
    }

    static async getAllPolizasByIdCliente(idCliente: number) {
        return await db.query(`
            SELECT registro.id               AS id,
                   cliente.nombre            AS nombre,
                   cliente.paterno           AS apellidoPaterno,
                   cliente.materno           AS apellidoMaterno,
                   registro.poliza           AS poliza,
                   registro.fechaInicio      AS fechaInicio,
                   registro.primaNeta        AS primaNeta,
                   registro.fechaRegistro    AS fechaRegistro,
                   registro.archivo          AS archivo,
                   registro.idSocio          AS idSocio,
                   tipoPago.cantidadPagos    AS cantidadPagos,
                   tipoPago.tipoPago         AS tipoPago,
                   productosSocio.nombre     AS productoSocio,
                   productosSocio.idSubRamo  AS idSubRamo,
                   tipoSubRamo.tipo          AS tipoSubRamo,
                   estadoPoliza.estado       AS estado,
                   productoCliente.idCliente AS idCliente,
                   cliente.archivoSubido     AS archivoSubido,
                   tipoRamo.tipo             AS tipoRamo,
                   socios.nombreComercial    AS alias,
                   productoCliente.datos     AS datos
            FROM ((((((((((
                registro
                    LEFT JOIN tipoPago ON ((
                    registro.idTipoPago = tipoPago.id
                    )))
                LEFT JOIN productosSocio ON ((
                    registro.idProductoSocio = productosSocio.id
                    )))
                LEFT JOIN subRamo ON ((
                    productosSocio.idSubRamo = subRamo.id
                    )))
                LEFT JOIN tipoSubRamo ON ((
                    subRamo.idTipoSubRamo = tipoSubRamo.id
                    )))
                LEFT JOIN ramo ON ((
                    subRamo.idRamo = ramo.id
                    )))
                LEFT JOIN tipoRamo ON ((
                    ramo.idTipoRamo = tipoRamo.id
                    )))
                LEFT JOIN productoCliente ON ((
                    registro.idProducto = productoCliente.id
                    )))
                LEFT JOIN estadoPoliza ON ((
                    registro.idEstadoPoliza = estadoPoliza.id
                    )))
                LEFT JOIN cliente ON ((
                    productoCliente.idCliente = cliente.id
                    )))
                     LEFT JOIN socios ON ((
                ramo.idSocio = socios.id
                )))
            WHERE ((
                       subRamo.idTipoSubRamo IN (1, 2, 7, 8, 15, 16)
                       )
                AND (
                       registro.idEstadoPoliza IN (4, 5, 6)))
              AND idCliente = ${idCliente}
            ORDER BY registro.id DESC
        `, {
            type: QueryTypes.SELECT,
        })
    }
}
