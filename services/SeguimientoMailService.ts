import SeguimientoMail from "../models/SeguimientoMail";
import db from "../db/connection";
import PasswordRecovery from "../models/PasswordRecovery";
import WebhookMailchimp from "../models/WebhookMailchimp";


export default class SeguimientoMailService {
    static async save(data: any) {
        return await SeguimientoMail.create(data);
    }

    static async update(data: any, idEmail: any) {
        return await SeguimientoMail.update(data, {
            where: {
                id: idEmail
            }
        });
    }

    static async getDataMail(idSolicitud) {
        return (await db.query(`SELECT
cliente.nombre,
cliente.paterno,
cliente.materno,
cliente.id as idCliente,
productoCliente.datos ->> '$.marca' AS marca,
productoCliente.datos ->> '$.modelo' AS modelo,
productoCliente.datos ->> '$.descripcion' AS descripcion,
productoCliente.datos ->> '$.detalle' AS detalle,
solicitudes.id,
registro.poliza,
cliente.correo,
cliente.contrasenaApp
FROM
cliente
INNER JOIN productoCliente ON productoCliente.idCliente = cliente.id
INNER JOIN solicitudes ON productoCliente.idSolictud = solicitudes.id
INNER JOIN registro ON registro.idProducto = productoCliente.id
WHERE
solicitudes.id = ${idSolicitud} LIMIT 1
`))[0];
    }

    static async recoveryPassword(data: any) {
        return await PasswordRecovery.create(data);
    }

    static async updateRecoveryPassword(data: any, idRecovery) {
        return await PasswordRecovery.update(data, {
            where: {
                id: idRecovery
            }
        });
    }

    static async findRecoveryById(idRecovery) {
        return await PasswordRecovery.findOne({
            where: {
                id: idRecovery
            }
        })
    }

    static async webhookMailchimp(data) {
        return await WebhookMailchimp.create({
            respuesta: data
        })
    }
}
